package com.me.rocketbunny;

import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.me.rocketbunny.actors.PipeCreator;


public class CreatorScreen extends RocketBunnyScreen {

	PipeCreator piper;

	public CreatorScreen(RocketBunny game) {
		super(game);
		this.piper = new PipeCreator();
		this.piper.addListener(new ClickListener() {
			@Override
            public void clicked(InputEvent event, float x, float y) {
				CreatorScreen.this.piper.nextPipe();
				super.clicked(event, x, y);
			}
		});
		this.stage.addActor(this.piper);
	}


}
