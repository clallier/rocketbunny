package com.me.rocketbunny;

import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.me.rocketbunny.actors.TextActor;


public class ScoreScreen extends RocketBunnyScreen {

	TextActor score;
	TextActor retry;

	public ScoreScreen(RocketBunny game) {
		super(game);

        this.score = new TextActor("Score is " + GV.score/2, GV.screen_width / 2 - GV.k * 2, GV.screen_height / 2);
		this.retry = new TextActor("touch to try again", GV.screen_width/2-GV.k*2, GV.screen_height/2-GV.k*2);
		this.retry.setBounds(0, 0, GV.screen_width, GV.screen_height);
        GV.score = 0;
		this.retry.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				GV.screen.game.gotoPlayerScreen();
				super.clicked(event, x, y);
			}
		});

		this.stage.addActor(this.score);
		this.stage.addActor(this.retry);
	}

}
