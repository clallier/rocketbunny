package com.me.rocketbunny.actors;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;

public class TextActor extends Actor {

	BitmapFont font;
	String text;
	Vector2 pos;
	
	public TextActor(String text, int x, int y) {
		font = new BitmapFont(Gdx.files.internal("data/tiny.fnt"));
		this.text = text;
		pos = new Vector2(x, y);
	}
	
	
	public void draw (Batch batch, float parentAlpha) {
		super.draw(batch, parentAlpha);
		font.draw(batch, text, pos.x, pos.y);
	}

}
