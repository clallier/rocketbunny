package com.me.rocketbunny.actors;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.me.rocketbunny.GV;

public class Bunny extends Actor {
    final float ACCELERATION_INCREMENT = 0.3f;
    final float JUMP_ACCELERATION = -10;
    final float PIPE_SPEED = -20;
    Texture bunnyTexture;
    public Sprite bunny;
    float acceleration = 0;

    public Bunny() {
        super();
        this.bunnyTexture = new Texture(Gdx.files.internal("data/bunny.png"));
        //this.bunnyTexture.setFilter(TextureFilter.Nearest, TextureFilter.Nearest);

        this.bunny = new Sprite(this.bunnyTexture);
        this.bunny.setPosition(100, 100);
        // setRegion(this.bunny);
        // setOrigin(bunny.getWidth() / 2, bunny.getHeight() / 2);
        this.bunny.setScale(3);
        setBounds(0, 0, GV.screen_width, GV.screen_height);
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        super.draw(batch, parentAlpha);
        this.bunny.draw(batch, parentAlpha);
    }

    @Override
    public void act(float delta) {
        super.act(delta);
        this.bunny.setY(this.bunny.getY() - this.acceleration);
        this.acceleration = this.acceleration + this.ACCELERATION_INCREMENT;
        float degrees;
        float angle;
        angle = MathUtils.atan2(this.PIPE_SPEED, this.acceleration);
        degrees = -(float)Math.toDegrees(angle) - 90;
        this.bunny.setRotation(degrees);
    }

    public void jump() {
        this.acceleration = this.JUMP_ACCELERATION;
    }

}
