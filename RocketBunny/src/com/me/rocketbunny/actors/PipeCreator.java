package com.me.rocketbunny.actors;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.utils.Array;
import com.me.rocketbunny.GV;
import com.me.rocketbunny.sprites.PipeBottom;
import com.me.rocketbunny.sprites.PipeCenter;
import com.me.rocketbunny.sprites.PipeTop;


public class PipeCreator extends Actor {

    Array<Group> pipes;
	int currentX;
    boolean falling = false;
    float TOP_START = 600;
    public Bunny bunny;
	//Sprite top, center, bottom;

	public PipeCreator() {
		super();
		/*
		top = new PipeTop();
		top .setPosition(0, GV.k*2);

		center = new PipeCenter();
		center.setPosition(0, GV.k);

		bottom = new PipeBottom();
		bottom.setPosition(0, 0);
		*/

		this.currentX = 0;
		this.pipes = new Array<Group>();
		for(int i=0; i<2; ++i)
			createPipeGroup();


	}

	public void createPipeGroup() {
		Group group = new Group();
		float holeSize = MathUtils.random(3, 10);
		int begin = MathUtils.floor(6 - holeSize/2);
		int end = MathUtils.floor(6 + holeSize/2);

		// parts
		boolean last = true;
		for(int i=0; i<12; ++i) {

			// nothing to add
			if(i> begin && i<=end) {
				last = false;
			}

			// add top block
			else if(i == begin) {
                Image top = new Image(new PipeTop());
				top.scaleBy(2);
				group.addActor(top);
                top.setPosition(GV.screen_width, (i + 1) * GV.k);
				last = true;
			}

			// add center block
			else if(last == true) {
                Image center = new Image(new PipeCenter());
				center.scaleBy(2);
				group.addActor(center);
                center.setPosition(GV.screen_width, (i + 1) * GV.k);
			}

			// add bottom block
			else if(i > end && last == false) {
                Image bottom = new Image(new PipeBottom());
				bottom.scaleBy(2);
				group.addActor(bottom);
                bottom.setPosition(GV.screen_width, (i + 1) * GV.k);
				last = true;
			}
		}

		group.setPosition(this.currentX, this.pipes.size*12*GV.k);
		this.pipes.add(group);
	}

	@Override
	public void act(float delta) {
		super.act(delta);
        for (Group g : this.pipes) {
            g.setX(g.getX() + GV.PIPE_SPEED);
            for (Actor p : g.getChildren()) {
                this.check_collision(p, g);
            }
            if (g.getX() + 80 < - GV.screen_width) {
                GV.score++;
                this.remove();
            }
        }
        if (!this.falling)
            return;
        for (Group g : this.pipes)
            g.setY(g.getY() - 40);

	}

	@Override
    public void draw (Batch batch, float parentAlpha) {
		super.draw(batch, parentAlpha);
		for(Group g: this.pipes)
			g.draw(batch, parentAlpha);

	}

	public void nextPipe() {
		this.currentX += 4*GV.k;
		this.pipes = new Array<Group>();
		for(int i=0; i<40; ++i)
			createPipeGroup();
	}

    private boolean check_collision(Actor pipe, Actor pipes) {
        float Ax = pipe.getX() + pipes.getX() + 4;
        float Ay = pipe.getY() + pipes.getY() + 4;
        float Bx = pipe.getX() + pipes.getX() + 56;// pipe.getWidth();
        float By = pipe.getY() + pipes.getY() + 56;// pipe.getHeight();

        float Cx = GV.screen.bunny.bunny.getX() + 4;
        float Cy = GV.screen.bunny.bunny.getY() + 4;
        float Dx = GV.screen.bunny.bunny.getX() + 56; // GV.screen.bunny.bunny.getWidth();
        float Dy = GV.screen.bunny.bunny.getY() + 56; // GV.screen.bunny.bunny.getHeight();
        // Random rn = new Random();
        if (Math.random() > 0.9) {
            if ((Bx > Cx && Ax < Dx && By > Cy && Ay < Dy) || Dy < 0) {
                GV.screen.game.gotoScoreScreen();
            }
        }
        return true;
    }
}
