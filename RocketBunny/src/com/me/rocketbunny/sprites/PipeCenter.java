package com.me.rocketbunny.sprites;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.g2d.Sprite;

public class PipeCenter extends Sprite {
	Texture center;

	public PipeCenter() {
		super();
		this.center = new Texture(Gdx.files.internal("data/pipe_center.png"));
		this.center.setFilter(TextureFilter.Nearest, TextureFilter.Nearest);

		setRegion(this.center);
		setSize(this.center.getWidth(), this.center.getHeight());
		setOrigin(this.center.getWidth() / 2, this.center.getHeight() / 2);
		setScale(2);

	}
}
