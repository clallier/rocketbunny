package com.me.rocketbunny.sprites;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.g2d.Sprite;

public class PipeBottom extends Sprite {
	Texture bottom;

	public PipeBottom() {
		super();
		this.bottom = new Texture(Gdx.files.internal("data/pipe_bottom.png"));
		this.bottom.setFilter(TextureFilter.Nearest, TextureFilter.Nearest);

		setRegion(this.bottom);
		setOrigin(this.bottom.getWidth() / 2, this.bottom.getHeight() / 2);
		setScale(2);

	}
}
