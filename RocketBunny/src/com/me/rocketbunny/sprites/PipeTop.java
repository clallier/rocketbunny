package com.me.rocketbunny.sprites;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.g2d.Sprite;

public class PipeTop extends Sprite {

	Texture top;

	public PipeTop() {
		super();
		this.top = new Texture(Gdx.files.internal("data/pipe_top.png"));
		this.top.setFilter(TextureFilter.Nearest, TextureFilter.Nearest);

		setRegion(this.top);
		setSize(this.top.getWidth(), this.top.getHeight());
		setOrigin(this.top.getWidth() / 2, this.top.getHeight() / 2);
		setScale(2);

	}
}
