package com.me.rocketbunny;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;


public class StartScreen extends RocketBunnyScreen {
	Texture titleTexture;
	
	Image titleImage;
	
	
	public StartScreen (RocketBunny rocketBunny) {
		super(rocketBunny);
		titleTexture = new Texture(Gdx.files.internal("data/title.png"));
		titleTexture.setFilter(TextureFilter.Nearest, TextureFilter.Nearest);
		
		titleImage = new Image(titleTexture);
		titleImage.setScale(6);
		titleImage.setOrigin(titleTexture.getWidth()/2, titleTexture.getHeight()/2);
		titleImage.setPosition(GV.screen_width/2, GV.screen_height/2);
		//titleImage.setBounds(0, 0, GV.screen_width, GV.screen_height);
		titleImage.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				game.gotoPlayerScreen();
				super.clicked(event, x, y);
			}
		});
		
		
		stage.addActor(titleImage);
		
		
		
	}
}
