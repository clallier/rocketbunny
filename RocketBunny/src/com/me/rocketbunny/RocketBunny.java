package com.me.rocketbunny;

import com.badlogic.gdx.Game;

/**
 * TOdo multi screen => 1 stage by screen
 * @author Godjam
 *
 */
public class RocketBunny extends Game {

	RocketBunnyScreen screen;

	@Override
	public void create() {
		screen = new StartScreen(this);
		setScreen(screen);
	}
	
	public void gotoPlayerScreen() {
		screen = new PlayerScreen(this);
		setScreen(screen);
	}
	
	public void gotoScoreScreen() {
		screen = new ScoreScreen(this);
		setScreen(screen);
	}
	
	
	@Override
	public void resume() {
		this.screen.resume();
	}

}
