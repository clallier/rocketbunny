package com.me.rocketbunny;

import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.me.rocketbunny.actors.Bunny;
import com.me.rocketbunny.actors.PipeCreator;


public class PlayerScreen extends RocketBunnyScreen {

    PipeCreator piper;
    public Bunny bunny;
    int pipe_countdown = 0;

    public PlayerScreen(RocketBunny game) {
        super(game);

        this.bunny = new Bunny();
        this.bunny.addCaptureListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                PlayerScreen.this.bunny.jump();
                super.clicked(event, x, y);
            }
        });


        this.stage.addActor(this.bunny);
        GV.screen = this;


    }

    @Override
    public void render(float delta) {
        super.render(delta);
        if (this.pipe_countdown == 0) {
            this.piper = new PipeCreator();
            this.stage.addActor(this.piper);
            this.pipe_countdown = GV.PIPE_FREQUENCY;
        }
        this.pipe_countdown--;
    }
}
